let users = {
    "_id": "1231sadf1",
    "firstName": "Marc Allen",
    "lastName": "Nanong",
    "email": "marcallen.nanong11@gmail.com",
    "password": "$password",
    "isAdmin": false,
    "mobileNo": "+639999999999"
}

let orders = {
    "_id": "32345sdf4f82344",
    "userId": "1231sadf1",
    "transactionDate": "2022-03-02T13:32:00",
    "Status": "Paid",
    "Total": 160000
}

let orderProducts = [
    {
        "_id": "sdf648456esdf8",
        "orderId": "32345sdf4f82344",
        "productId": "njh234145",
        "quantity": 2,
        "price": 40000,
        "subTotal": 80000
    },
    {
        "_id": "sdgfg2131243",
        "orderId": "32345sdf4f82344",
        "productId": "sdgfg2131243",
        "quantity": 1,
        "price": 80000,
        "subTotal": 80000
    },
]

let products = [
    {
        "_id": "njh234145",
        "name": "Google Pixel Pro 6",
        "Description": "Highend smartphone made by Google.",
        "Price": 40000,
        "stocks": 125,
        "isActive": true,
        "SKU": 897651548684,
    },
    {
        "_id": "sdgfg2131243",
        "name": "Iphone 16 Pro Max",
        "Description": "Highend smartphone made by Apple.",
        "Price": 80000,
        "stocks": 230,
        "isActive": true,
        "SKU": 635421848534,
    }
]


